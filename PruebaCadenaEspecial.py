'''
Created on 10 sep. 2021

@author: arman
'''
class CadenaEspecial:
    def __init__(self,cadena):
        self.cadena=""
        self.cadena=cadena
    def getCadena(self):
        return self.cadena
    def setCadena(self,cadena):
        self.cadena=cadena
        
    def mostrarCadenaInvertidaporLetraYPalabras(self):
        print("Cadena invertida")
        i=len(self.cadena)-1
        for i in range(i,-1,-1):
            print(self.cadena[i], end="")
        print()
        print("Cadena invertida por palabras")
        listaPalabras=self.cadena.split(" ")
        i=len(listaPalabras)-1
        for i in range(i,-1,-1):
            print(listaPalabras[i])

    def agregarEliminarCaracteresYSubcadenasPosicionesEspecificas(self,opcionUsuario):
        if(opcionUsuario=="A"):
            nuevaCad=""
            try:
                poci=int(input("Ingresa la posicion: "))
                i=0
                for i in range(i,len(self.cadena)):
                        if(i!=poci-1):
                            nuevaCad=nuevaCad+self.cadena[i]
                self.cadena=nuevaCad
            except ValueError:
                print("NO ingresaste un valor numerico")
            print(self.cadena)
                
        elif(opcionUsuario=="B"):
            try:
                poci=int(input(f"Ingresa el numero de la posicion donde se agregara el caracter: "))
                carac=input("Ingresa el caracter: ")
                nuevaCad=""
                i=0
                poci=poci-1
                if (poci<len(self.cadena)):
                    for i in range(i,len(self.cadena)):
                        if(i==poci):
                            nuevaCad=nuevaCad+carac
                        else:
                            nuevaCad=nuevaCad+self.cadena[i]
                    self.cadena=nuevaCad
                else:
                    print("La pocicion que ingresaste es no existe")
                    
            except ValueError:
                print("No ingresaste un numero")
            print(self.cadena)
            
        elif(opcionUsuario=="C"):
            subcad=input("Ingresa la posicion de la subcadena ejemplo (1-5): ")
            indices=[]
            indices=subcad.split("-")
            num1=int(indices[0])
            num2=int(indices[1])
            nuevaCad=""
            i=0
            if(num1<len(self.cadena) or num2<len(self.cadena)):
                for i in range(i,len(self.cadena)):
                    if(not(i>num1 and i<num2)):
                        nuevaCad=nuevaCad+self.cadena[i:i+1]
            else:
                print("La subcadena no existe")
            self.cadena=nuevaCad
            print("Subcadena eliminada")
            print(self.cadena)
        
    def mostrarCadenaCaMeLCaSe(self):
        i=0
        for i in range(i,len(self.cadena)):
            poci=self.cadena[i]
            if(i%2==0):
                print(poci.lower(),end ="")
            else:
                print(poci.upper(),end="")
                
    def mostrarCadenaMayusculasCadaPalabra(self):
        i=0
        numEspacios=0
        nuevaCad=""
        for i in range(i,len(self.cadena)):
            if(self.cadena[i]==" "):
                numEspacios=numEspacios+1
        if(numEspacios>0):
            poci=""
            listaPalabras=self.cadena.split(" ")
            i=0
            x=0
            nuevaCad=""
            for i in range(i,len(listaPalabras)):
                poci=listaPalabras[i]
                x=0
                for x in range(x,len(poci)):
                    if(x==0):
                        nuevaCad=nuevaCad+poci[x].upper()
                    else:
                        nuevaCad=nuevaCad+poci[x]
                nuevaCad=nuevaCad+" "
            self.cadena=nuevaCad
        else:
            self.cadena=self.cadena.title()
            print(self.cadena)
           
fraseDefault=input("Ingresa una cadena: ")

cad=CadenaEspecial(fraseDefault)
opcion=0
x=False
while (x==False):
    opcion=0
    print("Elige una opcion:")
    print("1-Mostrar cadena invertida y mostrarla por palabras")
    print("2-Agregar o eliminar Caracteres / Subcadenas en Pociciones Especificas")
    print("3-Mostrar cadena en formato CaMeL CaSe especial")
    print("4-Mostar la cadena con la primer letra de cada palabra en mayuscula")
    print("5-Salir")
    try:
        opcion=int(input())
    except ValueError:
        print("Debes de ingresar un numero")
        
    if(opcion==1):
        cad.mostrarCadenaInvertidaporLetraYPalabras()
    elif(opcion==2):
        print("Elige una opcion")
        print("A) Eliminar caracter")
        print("B) Agregar caracter")
        print("C) Eliminar Subcadena")
        menu2=input().upper()
        if(menu2=="A"):
            cad.agregarEliminarCaracteresYSubcadenasPosicionesEspecificas(menu2)
            print(cad.getCadena())
        elif(menu2=="B"):
            cad.agregarEliminarCaracteresYSubcadenasPosicionesEspecificas(menu2)
        elif(menu2=="C"):
            cad.agregarEliminarCaracteresYSubcadenasPosicionesEspecificas(menu2)
        else:
            print("Elige una opcion correcta")
    elif(opcion==3):
        cad.mostrarCadenaCaMeLCaSe()
        x=True
    elif(opcion==4):
        cad.mostrarCadenaMayusculasCadaPalabra()
        print(cad.getCadena())
    elif(opcion==5):
        print("Fin del programa")
    else:
        if(not opcion==0):
            print("opcion no disponible")